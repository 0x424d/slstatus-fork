#include <stdio.h>
#include <string.h>
#include "netspeed_all.h"

#define NETSPD_DEBUG

const char *
netspeed_all(const char *interface)
{
	char *tok;
	char new_interface[NETSPD_BUFFSZ];
	char *out_str;
	char sep[] = "./";
	const char *(*netspeedfunc)(const char*);

	strcpy(new_interface, interface);
	tok = strtok(new_interface, sep);

	#ifdef NETSPD_DEBUG
		printf("TOK: %s\n", tok);
	#endif

	if (!strcmp(tok, "RX")) {
		netspeedfunc = &netspeed_rx;
	} else if (!strcmp(tok, "TX")) {
		netspeedfunc = &netspeed_tx;
	} else {
		netspeedfunc = &ipv4;
	}

	tok = strtok(NULL, sep);
	while (tok != NULL) {
		#ifdef NETSPD_DEBUG
			printf("TOK: %s\n", tok);
		#endif
		if ((out_str = (char*)(*netspeedfunc)(tok)) != NULL) {
			#ifdef NETSPD_DEBUG
				printf("OUT_STR: %s\n", out_str);
			#endif
			return out_str;
		}
		tok = strtok(NULL, sep);
	}
	return "n/a";
}

#undef NETSPD_DEBUG
