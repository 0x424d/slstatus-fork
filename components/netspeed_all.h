#ifndef NETSPEEDS_ALL_H
#define NETSPEEDS_ALL_H

#define NETSPD_BUFFSZ 256
const char *netspeed_rx(const char *interface);
const char *netspeed_tx(const char *interface);
const char *ipv4       (const char *interface);
const char *netspeed_all(const char *interface);
#endif
