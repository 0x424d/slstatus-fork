#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define ACTIVENET_DEBUG

int activeNet(char output[1024]) {

	FILE *netDir;
	if ((netDir = popen("ls /sys/class/net","r")) == NULL){
		return 1;
	}
	FILE *checkstat;
	char cmd[1024] = {0};
	char status[1024] = {0};

	#ifdef ACTIVENET_DEBUG
		printf("FUNBEGIN\n");
	#endif	

	while (fgets(output, 1024, netDir)){
		#ifdef ACTIVENET_DEBUG
			printf("NETITEM: %s\n", output);
		#endif	

		output[strcspn(output, "\n")] = 0;
		sprintf(cmd, "cat /sys/class/net/%s/operstate", output);	

		#ifdef ACTIVENET_DEBUG
			printf("CMD: %s\n", cmd);
		#endif	
		checkstat = popen(cmd, "r");
		if(!fgets(status, sizeof(status), checkstat)) {
			pclose(checkstat);
			return 1;
		}	
		pclose(checkstat);
		#ifdef ACTIVENET_DEBUG
			printf("STATUS: %s\n", status);
		#endif	

		if (!strcmp("up\n", status)) {
			pclose(netDir);
			return 0;
		}		
	}
	strcpy(output,"lo");
	pclose(netDir);
	return 1;
}

